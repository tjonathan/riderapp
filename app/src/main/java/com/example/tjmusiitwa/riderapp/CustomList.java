package com.example.tjmusiitwa.riderapp;

/**
 * Created by TJ Musiitwa on 20-Oct-17.
 */

public class CustomList  {
    private String process;
    private int checkmark;

    public CustomList(String process, int checkmark) {
        this.process = process;
        this.checkmark = checkmark;
    }

    public String getProcess() {
        return process;
    }


    public int getCheckmark() {
        return checkmark;
    }


}
