package com.example.tjmusiitwa.riderapp;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by TJ Musiitwa on 20-Oct-17.
 */

public class CustomListAdapter extends ArrayAdapter<CustomList> {
    //Activity Context
    Context context;
    //The list of  values
    private List<CustomList> processList;
    //Layout resource for the checkmark
    private int resource;


    public CustomListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<CustomList> processList) {
        super(context, resource, processList);
        this.context = context;
        this.resource = resource;
        this.processList = processList;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if ( convertView == null){
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.custom_process_list_view_item,parent, false);
        }
        else{
            ((LinearLayout) convertView).removeAllViews();
        }

        for(int i= 0; i <=5; i++){
            View holder = inflater.inflate(R.layout.custom_process_list_view_item,parent, false);
            TextView customProcessName = (TextView) holder.findViewById(R.id.customProcessTextView);
            ImageView checkmark = view.findViewById(R.id.customProcessImageView);
        }


        //Getting the view
        //View view = inflater.inflate(resource, null, false);

        //getting the view elements of the list from the view
        TextView customProcessName = view.findViewById(R.id.customProcessTextView);
        ImageView checkmark = view.findViewById(R.id.customProcessImageView);

        CustomList process = processList.get(position);


        //adding values to the list item
        customProcessName.setText(process.getProcess());
        checkmark.setImageDrawable(context.getResources().getDrawable(process.getCheckmark()));


        //Return the view
        return view;
    }
}
