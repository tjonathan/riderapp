package com.example.tjmusiitwa.riderapp;

/**
 * Created by TJ Musiitwa on 17-Oct-17.
 */

public class DeliveryList {
    private String Docker_number;
    private String Delivery_status;
    private String Late_delivery;

    public DeliveryList(String docker_number, String delivery_status, String late_delivery) {
        Docker_number = docker_number;
        Delivery_status = delivery_status;
        Late_delivery = late_delivery;
    }

    public String getDocker_number() {
        return Docker_number;
    }

    public String getDelivery_status() {
        return Delivery_status;
    }

    public String getLate_delivery() {
        return Late_delivery;
    }
}
