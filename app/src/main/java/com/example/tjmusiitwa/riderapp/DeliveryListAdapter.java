package com.example.tjmusiitwa.riderapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by TJ Musiitwa on 17-Oct-17.
 */

public class DeliveryListAdapter extends RecyclerView.Adapter<DeliveryListAdapter.ViewHolder> {
    private List<DeliveryList> DeliveryList;
    private Context context;

    public DeliveryListAdapter(List<DeliveryList> deliveryList, Context context) {
        DeliveryList = deliveryList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_delivery_list_items, parent, false);
        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final DeliveryList deliveryList = DeliveryList.get(position);

        holder.textViewDocker.setText(deliveryList.getDocker_number());
        holder.textViewDeliveryStatus.setText(deliveryList.getDelivery_status());
        holder.textViewLateDelivery.setText(deliveryList.getLate_delivery());
        holder.linearCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "You clicked the order", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, RideDetails.class);
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return DeliveryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewDocker;
        public TextView textViewDeliveryStatus;
        public TextView textViewLateDelivery;
        public LinearLayout linearCard;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewDocker = itemView.findViewById(R.id.docker_number);
            textViewDeliveryStatus = itemView.findViewById(R.id.delivery_status);
            textViewLateDelivery = itemView.findViewById(R.id.late_delivery);
            linearCard = itemView.findViewById(R.id.linearCard);
        }

    }

}

