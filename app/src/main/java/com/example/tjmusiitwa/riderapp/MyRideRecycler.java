package com.example.tjmusiitwa.riderapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TJ Musiitwa on 17-Oct-17.
 */

public class MyRideRecycler extends AppCompatActivity {
    private RecyclerView deliveryRecyclerView;
    private DeliveryListAdapter deliveryAdapter;
    private List<DeliveryList> deliveryListItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_ride_activity);
        deliveryRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        deliveryRecyclerView.setHasFixedSize(true);
        deliveryRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        deliveryListItems = new ArrayList<>();

        for (int i = 0; i <= 1; i++) {
            DeliveryList deliveryListItem = new DeliveryList(
                    "Docker Number",
                    "Delivery Status",
                    "Late Delivery"
            );

            System.out.println("delivery list "+deliveryListItem);

            deliveryListItems.add(deliveryListItem);
        }

        deliveryAdapter = new DeliveryListAdapter(deliveryListItems, this);

        deliveryRecyclerView.setAdapter(deliveryAdapter);

    }

}

