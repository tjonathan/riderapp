package com.example.tjmusiitwa.riderapp;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;


/**
 * Created by TJ Musiitwa on 17-Oct-17.
 */

public class OrderDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        builder.setTitle("ORDER DETAILS")
                .setView(inflater.inflate(R.layout.order_dialog_fragment, null))

                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();


                    }
                });
        //Create the AlertDialog object and return it
        return builder.create();
        //return null;
    }


}
