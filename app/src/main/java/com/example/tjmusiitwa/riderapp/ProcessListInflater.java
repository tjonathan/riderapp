package com.example.tjmusiitwa.riderapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by TJ Musiitwa on 20-Oct-17.
 */

public class ProcessListInflater extends Activity {
    private ListView processListView;
    //private LinearLayout linear_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.process_list_view);
        //linear_layout = findViewById(R.id.custom_linear);
        processListView = findViewById(R.id.processListViewItem);

        for(int i= 1; i<=5; i++){
            View view = LayoutInflater.from(this).inflate(R.layout.process_list_view, null);
            TextView processName  = view.findViewById(R.id.customProcessTextView);
            ImageView checkmark = view.findViewById(R.id.customProcessImageView);

            processListView.addView(processName);
            processListView.addView(checkmark);


        }
    }
}
