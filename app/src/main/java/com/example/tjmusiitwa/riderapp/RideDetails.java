package com.example.tjmusiitwa.riderapp;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by TJ Musiitwa on 17-Oct-17.
 */

public class RideDetails extends AppCompatActivity {
    public LinearLayout processListView;
    ListView customListView;
    List<CustomList> processList;
    private ViewGroup processLinear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ride_details);
        processLinear = (ViewGroup) findViewById(R.id.custom_linear);

        processList = new ArrayList<>();
        customListView = (ListView) findViewById(R.id.processListViewItem);

        CustomListAdapter adapter = new CustomListAdapter(this, R.layout.custom_process_list_view_item, processList);
        customListView.setAdapter(adapter);


        //processListView = (LinearLayout) findViewById(R.id.processListView);
        //((ViewGroup)TextView.getParent()).removeView(TextView);


//Custom Toolbar displaying Back Button, Screen Title and Order Details Icon
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


    /*    LinearLayout processLayout = (LinearLayout) findViewById(R.id.process_layout);
        for (int i = 0; i < 5; i++) {

            TextView process = new TextView(this);
            process.setText("Order Delayed - 12:00\n");
            process.setGravity(Gravity.LEFT);
            process.setLayoutParams(new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, WRAP_CONTENT));
            processLayout.addView(process);

            RelativeLayout relative = (RelativeLayout) findViewById(R.id.process_relative);
            ImageView check = new ImageView(this);
            int resID = getResources().getIdentifier(String.valueOf(R.drawable.check), "drawable", getPackageName());
            check.setImageResource(resID);
            relative.setGravity(Gravity.RIGHT);
            relative.addView(check);

        }*/


        for (int i = 1; i <= 5; i++) {
            View view = LayoutInflater.from(this).inflate(R.layout.custom_process_list_view_item, null);
            TextView processName = view.findViewById(R.id.customProcessTextView);
            ImageView checkmark = view.findViewById(R.id.customProcessImageView);
            processListView.addView(processName);
            processListView.addView(checkmark);

        }


    }


    //Creating and displaying and Alert dialog to view the Order content
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dialog, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        int id = item.getItemId();
        if (id == R.id.order_items_button) {
            showAlertDialog();


        } else {
            NavUtils.navigateUpFromSameTask(this);
        }
        return super.onOptionsItemSelected(item);
    }


    public void showAlertDialog() {

        OrderDialog orderDialog = new OrderDialog();
        orderDialog.show(getFragmentManager(), "ORDER DETAILS");
    }


}
